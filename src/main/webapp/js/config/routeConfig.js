/**
 * Criando rotas
 */
angular.module("listaTelefonica").config(function($routeProvider){
	$routeProvider
		.when("/contatos",{
			templateUrl: "views/contatos.html",
			controller: "ListaTelefonicaCtrl",
		})
		.when("/novoContato", {
			templateUrl: "views/novoContato.html",
			controller: "novoContatoCtrl",
			resolve: {
				operadoras : function(operadoraAPI) {
					return operadoraAPI.getOperadoras();
				},
				cores : function(corAPI) {
					return corAPI.getCores();
				}
			}
		}).when("/detalhesContato/:id",{
			templateUrl: "views/detalhesContato.html",
			controller: "detalhesContatoCtrl",
			resolve: {
				contato: function(contatoAPI, $route) {
					return contatoAPI.getContato($route.current.params.id);
				}
			},
				
		})
		.otherwise("/contatos");
}); 