/**
 * Script responsável para configurar propriedade,
 * nesse caso o length do seriaGeneratorService
 */
angular.module("listaTelefonica").config(function(serialGeneratorProvider){
	console.info("Valor do length do serial: " + serialGeneratorProvider.getLength());
	console.info("Alterando o valor do serial!");
	serialGeneratorProvider.setLength(100);
	
	console.info("Novo valor do length do serial: " + serialGeneratorProvider.getLength());
});