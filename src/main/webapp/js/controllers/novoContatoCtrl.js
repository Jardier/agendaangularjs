// Controllers
myApp.controller("novoContatoCtrl", function($scope, contatoAPI,
		 $location, operadoras, cores, Flash, util) {

	$scope.operadoras = operadoras.data;
	$scope.cores = cores.data;
	$scope.app = "Cadastro de Contatos"
	
	//CARREGAR OPERADORAS
//	var carregarOperadoras = function() {
//		operadoraAPI.getOperadoras().success(function(_operadoras) {
//			$scope.operadoras = _operadoras;
//			console.info("Operadoras carregadas com sucesso");
//
//		}).error(function(status) {
//			console.error("Ocorreu um erro ao carregas as operadoras!");
//		});
//	}
	
	// Adicionar contatos
	$scope.adicionarContatos = function(contato) {
		contatoAPI.savarContato(contato).success(function(_contato) {
			Flash.create('success', util.getMenssagem('success', 'Contato salvo com sucesso.'));
			console.info("Contato: " + _contato.nome + " salvo com sucesso.");
			$location.path("/contatos");
			
		}).error(function(_contato, status) {
			console.error("Ocorreu um erro ao salvar o Contato: " + _contato);
			Flash.create('danger', util.getMenssagem('danger', 'Ocorreu um erro ao salvar o contato!'));
		})
	}

	//carregarOperadoras(); //colocado no resolve
});
