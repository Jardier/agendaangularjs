// Controllers
myApp.controller("ListaTelefonicaCtrl", function($scope, operadoraAPI, contatoAPI, serialGenerator, $http, config,
		Flash, util) {
	$scope.app = "Lista Telefônica";
	
	
	
	//carregar Contatos
	var carregarContatos = function() {
		contatoAPI.getContatos().success(function(data){
			console.log(data);
			$scope.contatos = data;
		}).error(function(data){
			Flash.create('danger', util.getMenssagem('danger', 'Ocorreu um erro ao carrecar contatos.'));
			$scope.error = "Ocorreu um erro ao carrecar contatos";
		});
	}
	
	// Remover contatos
	$scope.apagarContatos = function(contatos) {
		$scope.contatos = contatoAPI.removeContatos(contatos);
	}
	
	$scope.apagarContato = function(contato) {
		contatoAPI.removeContato(contato).success(function(data){
			console.info("Contatos removido com sucesso...");
			Flash.create('success', util.getMenssagem('success', 'Contato removido com sucesso.'));
			carregarContatos();
		}).error(function(data) {
			Flash.create('danger', util.getMenssagem('danger', 'Ocorreu um erro ao remover o contato.'));
			console.error("Ocorreu um erro ao remover o contato " + contato.nome);
		})
	}
	// Verificar se existe um contato selecionado
	$scope.isContatoSelecionado = function(contatos) {
		return	contatoAPI.verificaContatoSelecionado(contatos);

	}
	
	// Ordenar lista
	$scope.ordenarPor = function(campo) {
		$scope.criterioDeOrdenacao = campo;
		$scope.direcaoDaOrdenacao = !$scope.direcaoDaOrdenacao;
	}
	
	//gerando um serial
	console.info("Gerenado um serial: " + serialGenerator.generate());
	carregarContatos();
	
});


