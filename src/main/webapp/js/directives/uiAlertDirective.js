/**
 * Script responsável por criar uma directive do tipo Templete alert
 */
angular.module("listaTelefonica").directive("uiAlert", function() {
	return{
		templateUrl: "views/alert.html",
		replace: true,
		restrict: "AE",
		scope: {
			title: "@title",
			message: "=message"
		}
	};
});