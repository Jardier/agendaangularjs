/**
 * Javascript auxiliar
 */
angular.module("util",[])
angular.module("util").provider("util", function() {
	var _length = 10;
	
	
	this.$get = function() {
		return {
			getMenssagem: function(type, texto) {
				var mensagem = "";
				if(type === 'success') {
					mensagem = '<strong><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span></strong> ' + texto;	
				}else if(type === 'danger') {
					mensagem = '<strong><span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span></strong> ' + texto;
				}else if(type === 'info') {
					mensagem = '<strong><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></strong> ' + texto;
				}else if(type === 'warning') {
					mensagem = '<strong><span class="glyphicon glyphicon-alert" aria-hidden="true"></span></strong> ' + texto;
				}
				
				
				
				return mensagem;
			}
		};
	};


});