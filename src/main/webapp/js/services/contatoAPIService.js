/**
 * Script reponsável pelos serviços de Contato
 * criado com factory
 */
myApp.factory("contatoAPI", function($http, config) {
	var _getContatos = function() {
		return $http.get("http://localhost:8080/agendaangularjs/rs/contatos/all");
	};
	
	var _getContato = function(id) {
		return $http.get("http://localhost:8080/agendaangularjs/rs/contatos/" + id);
	}
	var _saveContato = function(contato) {
		return $http.post("http://localhost:8080/agendaangularjs/rs/contatos", angular.toJson(contato));
	}
	
	var _removeContato = function(contatos) {
		var contatosSelecionado = contatos.filter(function(contato) {
			if (!contato.selecionado)
				return contato;
		});
		return contatosSelecionado;
	};
	
	var _removerContato = function(contato) {
		return $http.delete(config.baseUrl + "/contatos/" + contato.id);
	}
	
	var _verificaContatoSelecionado = function(contatos){
		retorno = false;
		if(contatos != undefined) {
			retorno = contatos.some(function(contato) {
				return contato.selecionado;
			});
		}
		return retorno;
	}
	
	return {
		getContatos : _getContatos,
		removeContatos: _removeContato,
		verificaContatoSelecionado : _verificaContatoSelecionado,
		savarContato : _saveContato,
		getContato: _getContato,
		removeContato : _removerContato,
	};
	
});