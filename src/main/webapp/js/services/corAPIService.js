/**
 * Script reponsável pelos serviços de Cores
 * Criado com Service
 */
myApp.service('corAPI', function($http, config) {
	var _getCores = function() {
		return $http.get(config.baseUrl + '/cores/all');
	};
	
	return {
		getCores : _getCores,
	}
})