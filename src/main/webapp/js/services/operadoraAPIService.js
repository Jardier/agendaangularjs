/**
 * Script reponsável pelos serviços de Operadora
 * Criado com Service
 */
myApp.service("operadoraAPI", function($http, config) {
	this.getOperadoras = function() {
		return $http.get(config.baseUrl + "/operadoras/all");
	}
});