package br.com.agenda.restful.resouces;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.agenda.model.Operadora;

@Path("/operadoras")
@SuppressWarnings({ "rawtypes", "unused" })
public class OperadoraResource {
	private static List<Operadora> OPERADORAS = new ArrayList<Operadora>();
	private static Integer idOperadora = 5;

	static {
		Operadora oiCel = new Operadora(1, "Oi", 14, "Celular", 1.0);
		Operadora timCel = new Operadora(2, "Tim", 41, "Celular", 2.5);
		Operadora vivoCel = new Operadora(3, "Vivo", 15, "Celular", 3.0);
		Operadora gvtFixo = new Operadora(4, "GVT", 71, "Fixo", 0.85);
		Operadora embratelFixo = new Operadora(5, "Embratel", 81, "Fixo", 1.75);

		OPERADORAS.add(oiCel);
		OPERADORAS.add(timCel);
		OPERADORAS.add(vivoCel);
		OPERADORAS.add(gvtFixo);
		OPERADORAS.add(embratelFixo);

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/all")
	public List getOperadoras() {
		return new ArrayList<Operadora>(OPERADORAS);
	}

	public static Operadora fromId(Integer id) {
		for(Operadora operadora : OPERADORAS) {
			if(id.equals(operadora.getId()))
					return operadora;
		}
		
		return null;
	}
}
