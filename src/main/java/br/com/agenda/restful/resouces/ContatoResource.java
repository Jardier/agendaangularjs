package br.com.agenda.restful.resouces;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.agenda.model.Contato;
import br.com.agenda.model.Cor;

@Path("/contatos")
@SuppressWarnings({ "rawtypes" })
public class ContatoResource {
	private static Map<Integer, Contato> CONTATOS = new HashMap<Integer, Contato>();
	private static Integer idContato = 6;

	static {

		CONTATOS.put(1, new Contato(1, "JARDIER BEZERRA AIRES", "9 96218413", Cor.RED, new Date(), OperadoraResource.fromId(1)));
		CONTATOS.put(2, new Contato(2, "BÁRBARA DAS BEZERRA AIRES", "9 88696945", Cor.PINK, new Date(), OperadoraResource.fromId(2)));
		CONTATOS.put(3, new Contato(3, "Adriana DA COSTA AIRES", "9 87489166", Cor.YELLOW, new Date(), OperadoraResource.fromId(3)));
		CONTATOS.put(4, new Contato(4, "Jardier  DE Bezerra", "85 32574442", Cor.GREY, new Date(), OperadoraResource.fromId(4)));
		CONTATOS.put(5, new Contato(5, "Bárbara DAS Alves", "85 32574445", Cor.BLUE, new Date(), OperadoraResource.fromId(5)));
		CONTATOS.put(6, new Contato(6, "Bárbara DO Alves", "85 32574445", Cor.GREEN, new Date(), OperadoraResource.fromId(3)));
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/all")
	public List getContatos() {
		return new ArrayList<Contato>(CONTATOS.values());
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Contato getContato(@PathParam("id") Integer id) {
		Contato contatoConsulta = CONTATOS.get(id);
		System.out.println(contatoConsulta);
		return contatoConsulta;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Contato save(Contato contato) {
		idContato ++;
		contato.setId(idContato);
		CONTATOS.put(idContato, contato);
		System.out.println(CONTATOS.size());
		return contato;

	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public void remove(@PathParam("id") Integer id) {
		CONTATOS.remove(id);

	}

}
