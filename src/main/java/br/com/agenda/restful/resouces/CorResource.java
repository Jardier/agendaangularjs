package br.com.agenda.restful.resouces;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.agenda.model.Cor;

@SuppressWarnings("rawtypes")
@Path("/cores")
public class CorResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/all")
	public List getCores(){
		return Arrays.asList(Cor.values());
	}
	
	
	
	
}
