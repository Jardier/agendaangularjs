package br.com.agenda.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Operadora {
	private Integer id;
	private String nome;
	private int codigo;
	private String categoria;
	private double preco;
	
	public Operadora() {
		
	}
	@JsonCreator
	public Operadora(@JsonProperty("id")Integer id,@JsonProperty("nome") String nome, 
			@JsonProperty("codigo")int codigo,@JsonProperty("categoria") String categoria,@JsonProperty("preco") double preco) {
		super();
		this.id = id;
		this.nome = nome;
		this.codigo = codigo;
		this.categoria = categoria;
		this.preco = preco;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Operadora [id=" + id + ", nome=" + nome + ", codigo=" + codigo + ", categoria=" + categoria + ", preco=" + preco + "]";
	}
	
	
}
