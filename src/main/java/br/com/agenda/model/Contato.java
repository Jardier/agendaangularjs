package br.com.agenda.model;

import java.util.Date;


public class Contato {

	private Integer id;
	private String nome;
	private String telefone;
	private Cor cor;
	private Date data;
	private Operadora operadora;
	
	public Contato() {
		
	}
	public Contato(int id, String nome, String telefone, Cor cor, Date data, Operadora operadora) {
		super();
		this.id = id;
		this.nome = nome;
		this.telefone = telefone;
		this.cor = cor;
		this.data = data;
		this.operadora = operadora;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public Cor getCor() {
		return cor;
	}
	public void setCor(Cor cor) {
		this.cor = cor;
	}
	
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Operadora getOperadora() {
		return operadora;
	}
	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}


	@Override
	public String toString() {
		return "Contato [id=" + id + ", nome=" + nome + ", telefone=" + telefone + ", cor=" + cor + ", date=" + data + ", operadora=" + operadora + "]";
	}
}
